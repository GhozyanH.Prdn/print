<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaksi extends CI_Model {

    public function isicombo($jenisukuran){
        $isi = $this->db->query('select * from ukuran where jenis_ukuran = "'.$jenisukuran.'"');
        return $isi->result_array();
    }
     public function getHarga($jenisukuran,$ukuran){
        $isi = $this->db->query('select * from ukuran where jenis_ukuran = "'.$jenisukuran.'" and nama_ukuran = "'.$ukuran.'"');
        return $isi->result_array();
    }

    public function getTabel(){
        $isi = $this->db->query('select * from ukuran');
        return $isi->result_array();
    }
    
    public function getTabel2($where){
        $isi = $this->db->query('select * from ukuran'.$where);
        return $isi->result_array();
    }
    

}
