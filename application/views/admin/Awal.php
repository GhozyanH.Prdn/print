<div class="content-wrapper">
    <!-- Contenyt Header (Page header) -->
    <section class="content-header">
        <h1>
            CRUD Tabel Harga
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Admin</a></li>
            <li class="active">Harga</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">

                <!-- Horizontal Form -->
                <div class="box box-info">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url() . "index.php/admin/awal/do_insert" ?>" method="POST">
                        <div class="box-header with-border">
                            <h3 class="box-title" name="jumlah2">Insert Harga</h3>

                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label" >ukuran</label>

                                <div class="col-sm-10">
                                    <input type="text" name="nama_ukuran" class="form-control" id="inputPassword3" placeholder="Tulis nama surat">
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Jenis</label>
                            <div class="col-sm-10">
                                <select class="form-control select2" name="jenis_ukuran" style="width: 100%;" id="comboayat">
                                     <option>Spanduk/Banner</option>
                                     <option>Dokumen</option>
                                     <option>Poster</option>
                                     <option>Foto</option>
                                     <option>Kartu_nama</option>
                                     <option>Sertifikat</option>
                                     <option>Stiker</option>
                                        <option>X_Banner</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group">

                            <label for="inputEmail3" class="col-sm-2 control-label">harga</label>

                            <div class="col-sm-10">

                                <input type="text" name="harga" class="form-control" id="inputPassword3" placeholder="Jumlah ayat pada surat tsb">


                            </div>
                        </div>
                        
                        <br>
                        <br>
                        <?php echo $this->session->flashdata('pesan');?>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-info pull-right">Save!</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
                
               
            <!--/.col (right) -->
            </div>
            <div class="col-md-6">
                <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Surat</h3>
            </div>
                    
                    &nbsp;&nbsp; <?php echo $this->session->flashdata('pesan2');?>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Ukuran</th>
                  <th>Jenis</th>
                  <th>Harga</th>
                   <th>Action</th>
                </tr>
                
                
                 <?php foreach ($isi as $d){?>
                <tr>
                
                  <td><?php echo $d['nama_ukuran'] ?></td>
                  <td><?php echo $d['jenis_ukuran'] ?></td>
                  <td>
                    <?php echo $d['harga'] ?>
                  </td>
                  <td><a href="<?php echo base_url()."index.php/admin/awal/edit_data/".$d['id_ukuran']; ?>">Edit</a> | <a href="<?php echo base_url()."index.php/admin/awal/do_delete/".$d['id_ukuran']; ?>">Delete</a></td>
                </tr>
                 <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
            
          </div>
            </div>
        </div>
        
</div>

<!-- /.row -->
</section>
    
<!-- /.content -->
