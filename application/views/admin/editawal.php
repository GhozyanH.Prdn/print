<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Harga
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Admin</a></li>
            <li class="active">Harga</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->

            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">

                <!-- Horizontal Form -->
                <div class="box box-info">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="<?php echo base_url() . "index.php/admin/awal/do_update" ?>" method="POST">
                        <div class="box-header with-border">
                            <h3 class="box-title" name="jumlah2">Masukkan Harga yang ingin diganti</h3>

                        </div>
                        <div class="box-body">
                            <input type="hidden" name="id_ukuran" value="<?php echo $id_ukuran ?>">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label" >Nama</label>

                                <div class="col-sm-10">
                                    <input type="text" name="nama_ukuran" class="form-control" id="inputPassword3" placeholder="Tulis nama surat" value="<?php echo $nama_ukuran ?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Jenis</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2" name="jenis_ukuran" style="width: 100%;" id="comboayat">
                                        <option><?php echo $jenis_ukuran ?></option> 
                                        <option>Spanduk/Banner</option>
                                        <option>Dokumen</option>
                                        <option>Poster</option>
                                        <option>Foto</option>
                                        <option>Kartu_nama</option>
                                        <option>Sertifikat</option>
                                        <option>Stiker</option>
                                        <option>X_Banner</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label" >Harga</label>

                                <div class="col-sm-10">
                                    <input type="text" name="harga" class="form-control" id="inputPassword3" placeholder="Tulis nama surat" value="<?php echo $harga ?>"> 
                                </div>
                            </div>

                            <br>
                            <br>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="button" class="btn btn-default"><a href="<?php echo base_url(); ?>index.php/admin2/ayat">Cancel</a></button>
                                <button type="submit" class="btn btn-info pull-right">Save!</button>
                            </div>
                            <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>

            <!--/.col (right) -->
        </div>
</div>
</div>
<!-- /.row -->