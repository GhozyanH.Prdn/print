<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Online Printing</title>
<link rel="icon" href="<?php echo base_url('aset/images/logo.png'); ?>">
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="prezzie Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="<?php echo base_url(); ?>aset/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome-icons -->
<link href="<?php echo base_url(); ?>aset/css/style1.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url(); ?>aset/css/pendaftaran.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url(); ?>aset/css/font-awesome.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>aset/css/kalkulator.css" rel="stylesheet" type="text/css" media="all" />
<!-- //font-awesome-icons -->
<link href="<?php echo base_url(); ?>aset/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>aset/css/style.css" rel="stylesheet" type="text/css" media="all" />


<link href="//fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- banner -->
<div class="banner_top" id="home">
  <div data-vide-bg ="<?php echo base_url(); ?>aset/video/gift-packs">
    <div class="center-container inner-container">
      <div class="w3_agile_header">
            <div class="w3_agileits_logo">
                <h1><a href="index.html">Printing Online<span>Life is a gift</span></a></h1>
              </div>
              <div class="w3_menu">
              <div class="agileits_w3layouts_banner_info">
              </div>
                <div class="top-nav">
                <nav class="navbar navbar-default">
                  <div class="container">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu           
                    </button>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                    </ul> 
                    <div class="clearfix"> </div>
                  </div>  
                </nav>  
              </div>
            </div>

          <div class="clearfix"></div>
          </div>
        <!-- banner-text -->
      <h2 class="inner-heading-agileits-w3layouts">بِسْــــــــــــــــــمِ اللهِ الرَّحْمَنِ الرَّحِيْمِ</h2>
      <!--banner Slider starts Here-->
      </div>
   </div>
     </div>
<!-- //banner -->
<!-- about -->
  
<!-- gallery -->
<!-- //gallery -->
<?php echo $contents;?>
<!-- testimonials -->
<!-- //testimonials -->
<!--footer-->
    <div class="copyright">
             <p>© 2018 Information System UINSA. All rights reserved | Design by <a href="http://w3layouts.com">Kelompok 1</a></p>
        </div>
<!-- Modal1 -->
            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
              <div class="modal-dialog">
              <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Prezzie</h4>
                    <img src="images/f2.jpg" alt=" " class="img-responsive">
                    <h5>Integer lorem ipsum dolor sit amet </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, rds which don't look even slightly believable..</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- //Modal1 -->

<!--//footer-->

  <!-- js -->
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- for-Clients -->
    <script src="<?php echo base_url(); ?>aset/js/owl.carousel.js"></script>
      <!-- requried-jsfiles-for owl -->
                      <script>
                      $(document).ready(function() {
                        $("#owl-demo2").owlCarousel({
                          items : 1,
                          lazyLoad : false,
                          autoPlay : true,
                          navigation : false,
                          navigationText :  false,
                          pagination : true,
                        });
                      });
                    </script>
      <!-- //requried-jsfiles-for owl -->
  <!-- //for-Clients -->
 <script type="text/javascript">
          $(window).load(function() {
            $("#flexiselDemo1").flexisel({
              visibleItems: 4,
              animationSpeed: 1000,
              autoPlay: false,
              autoPlaySpeed: 3000,        
              pauseOnHover: true,
              enableResponsiveBreakpoints: true,
              responsiveBreakpoints: { 
                portrait: { 
                  changePoint:568,
                  visibleItems: 1
                }, 
                landscape: { 
                  changePoint:640,
                  visibleItems:2
                },
                tablet: { 
                  changePoint:768,
                  visibleItems: 3
                }
              }
            });
            
          });
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.flexisel.js"></script>
<!-- cart-js -->
  <script src="<?php echo base_url(); ?>aset/js/minicart.min.js"></script>
  <script>
    // Mini Cart
    paypal.minicart.render({
      action: '#'
    });

    if (~window.location.search.indexOf('reset=true')) {
      paypal.minicart.reset();
    }
  </script>
<!-- //cart-js --> 
<!-- video-bg -->
<script src="<?php echo base_url(); ?>aset/js/jquery.vide.min.js"></script>
<!-- //video-bg -->
<!-- Nice scroll -->
<script src="<?php echo base_url(); ?>aset/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>aset/js/scripts.js"></script>
<!-- //Nice scroll -->
<!-- for bootstrap working -->
<script type="text/javascript" src="<?php echo base_url(); ?>aset/js/bootstrap-3.1.1.min.js"></script>
<!-- //for bootstrap working -->
</body>
</html>