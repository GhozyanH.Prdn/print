<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="bs-example">
    <h1 style='text-align: center;'>Edit Profil</h1>
    <form  class="form-horizontal" <?php echo form_open_multipart('daftar/edit_profil'.$data['id']);?>
        <input type="hidden" name="level" value="pengguna">
        <div class="form-group">
            <label class="control-label col-xs-2" for="inputEmail">Email:</label>
            <div class="col-xs-8">
                <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email Anda" value="<?php echo $data ['email'];?>">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="inputPassword">Kata Sandi:</label>
            <div class="col-xs-8">
                <input type="password" name="password" class="form-control" id="inputPassword" value="<?php echo $data ['password'];?>" placeholder="Masukan Kata Sandi">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Awal:</label>
            <div class="col-xs-8">
                <input type="text" name="nama_awal" class="form-control" id="Namaawal" value="<?php echo $data ['nama_awal'];?>" placeholder="Nama Awal Anda">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Nama">Nama Akhir:</label>
            <div class="col-xs-8">
                <input type="text" name="nama_akhir" class="form-control" id="Namaakhir" value="<?php echo $data ['nama_akhir'];?>" placeholder="Nama Akhir Anda">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="telp">No. Telp:</label>
            <div class="col-xs-8">
                <input type="tel" name="telp" class="form-control" id="telp" value="<?php echo $data ['telp'];?>" placeholder="Nomor Telepon / Handphone">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="Alamat">Alamat:</label>
            <div class="col-xs-8">
                <textarea rows="8" name="alamat" class="form-control" id="Alamat" value="<?php echo $data ['alamat'];?>" placeholder="Masukan Alamat Lengkap"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2" for="KodePos">Kode Pos:</label>
            <div class="col-xs-8">
                <input type="text" name="pos" class="form-control" id="KodePos" value="<?php echo $data ['pos'];?>" placeholder="Kode Pos">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2">Jenis Kelamin:</label>
            <div class="col-xs-8">
                <input type="text" name="pos" class="form-control" id="KodePos" value="<?php echo $data ['jk'];?>" placeholder="Kode Pos">
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-2" for="foto">Photo Profil:</label>
            <div class="col-xs-8">
                <input type="file" name="userfile" value="<?php echo $data ['foto'];?>">
            </div>
        <div class="form-group">
        </div>
        <br>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-8">
                <input type="submit" class="btn btn-primary" value="Edit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div>
</body>
</html>                            