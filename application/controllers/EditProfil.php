<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EditProfil extends CI_Controller {

    public function index() {
    	$username = $this->session->userdata('username');
    	$data = $this->M_register->get_user($username);
    	$profil = array(
    		"nama_awal" => $data[0]["nama_awal"],
            "nama_akhir" => $data[0]["nama_akhir"],
    		"telp" => $data[0]["telp"],
    		"alamat" => $data[0]["alamat"],
            "pos" => $data[0]["pos"],
    		"email" => $data[0]["email"],
            "password" => $data[0]["password"],
    		"jk" => $data[0]["jk"],
    		"foto" => $data[0]["foto"]
    	);
		$this->template->load('static','EditProfil', $profil);
	}

    public function edit(){
        $config['upload_path']          = './aset/images/daftarbaru/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 5000;
        $config['max_height']           = 5000;
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload())
        {
            $this->load->view('V_error');
            
        }
        else{
            $img = $this->upload->data();
            $foto = $img['file_name'];
            $nama_awal = $this->input->post('nama_awal');
            $nama_akhir = $this->input->post('nama_akhir');
            $email = $this->input->post('email');
            $alamat = $this->input->post('alamat');
            $pos = $this->input->post('pos');
            $telp = $this->input->post('telp');
            $jk = $this->input->post('jk');
            $password = $this->input->post('password');
            $level = $this->input->post('level');

            $data = array(
                'nama_awal' => $nama_awal,
                'nama_akhir' => $nama_akhir,
                'email' => $email,
                'alamat' => $alamat,
                'pos' => $pos,
                'telp' => $telp,
                'jk' => $jk,
                'foto' => $foto,
                'password' => md5($password),
                'level' => $level,
            );
            $email = $this->session->userdata('username');
            $where = array('email' => $email);
            $this->db->update('user',$data,$where);
            redirect('Profil');
            
        }
    }
}