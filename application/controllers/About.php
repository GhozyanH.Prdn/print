<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	public function index()
	{
		if ($this->session->userdata('isPengguna') == TRUE) {
			$username = $this->session->userdata('username');
    	$data = $this->M_register->get_user($username);
    	$profil = array(
    		"nama_awal" => $data[0]["nama_awal"],
    		"telp" => $data[0]["telp"],
    		"alamat" => $data[0]["alamat"],
    		"email" => $data[0]["email"],
    		"jk" => $data[0]["jk"],
    		"foto" => $data[0]["foto"]
    	);
            $this->template->load('static', 'about', $profil);
        } else {
            redirect(login);
        }
	}
}
