<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {

        if ($this->session->userdata('isPengguna') == TRUE) {
            redirect('Gallery');
        } elseif ($this->session->userdata('isAdmin') == TRUE) {
            redirect('admin/awal');
        } else {
            $this->template->load('static1', 'login');
        }
    }

    public function do_login() {

        $username = $this->input->post('email');
        $password = md5($this->input->post('password'));
        $cek = $this->M_login->proseslogin($username, $password);
        $hasil = count($cek);
        if ($hasil > 0) {
            $pelogin = $this->db->get_where('user', array('email' => $username, 'password' => $password))->row();
            if ($pelogin->level == 'pengguna') {

                $this->session->set_userdata(array(
                    'isPengguna' => TRUE, //set data telah login
                    'username' => $username, //set session username
                        //'id_group' => $id_group, //set session hak akses
                ));
              
                redirect('Gallery');
            } elseif ($pelogin->level == 'admin') {
                $this->session->set_userdata(array(
                    'isAdmin' => TRUE, //set data telah login
                    'username2' => $username, //set session username
                        //'id_group' => $id_group, //set session hak akses
                ));
                echo "pengguna";
                echo $this->session->userdata('username2');
                redirect('admin/Awal');
            }

            //echo $pelogin->id;
            //redirect('admin');
        } else {
            $this->template->load('static1', 'login');
        }
    }

}
