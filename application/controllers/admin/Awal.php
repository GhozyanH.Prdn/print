<?php

Class Awal extends CI_Controller {

    function __construct() {
        parent::__construct();
        //chekAksesModule();
        //$this->load->library('ssp');
        //$this->load->model('Model_siswa');
    }

    function index() {

        if ($this->session->userdata('isAdmin') == TRUE) {
         $username = $this->session->userdata('username2');
            $data = $this->M_register->get_user($username);
            $profil = array(
                "nama_awal" => $data[0]["nama_awal"],
            );
        $isi = $this->M_transaksi->getTabel();
        $this->template->load('admin/static', 'admin/Awal', array('isi' => $isi, "nama_awal" => $data[0]["nama_awal"]));
        } else {
            redirect(login);
        }
    }

    public function do_insert() {
        $nama_ukuran = $_POST['nama_ukuran'];
        $jenis_ukuran = $_POST['jenis_ukuran'];
        $harga = $_POST['harga'];
        $data_insert = array(
            'nama_ukuran' => $nama_ukuran,
            'jenis_ukuran' => $jenis_ukuran,
            'harga' => $harga,
        );

        $res = $this->db->insert('ukuran', $data_insert);
        if ($res >= 1) {
            $this->session->set_flashdata('pesan', 'Data berhasil ditambah!');
            redirect('admin/awal');
        } else {
            echo "gagal";
        }
    }

    public function do_delete($nama_ayat) {
        $where = array('id_ukuran' => $nama_ayat);
        $res = $this->db->delete('ukuran', $where);
        if ($res >= 1) {
            $this->session->set_flashdata('pesan2', 'Data berhasil dihapus!');
            redirect('admin/awal');
        } else {
            echo "gagal";
        }
    }

    public function edit_data($nama_ayat) {

        $ayat = $this->M_transaksi->getTabel2(" where id_ukuran = '" . $nama_ayat . "'");
        $data = array(
            "nama_ukuran" => $ayat[0]["nama_ukuran"],
            "jenis_ukuran" => $ayat[0]["jenis_ukuran"],
            "harga" => $ayat[0]["harga"],
            "id_ukuran" => $nama_ayat
        );
        $this->template->load('admin/static', 'admin/editawal', $data);
    }

    public function do_update() {
        $id_ukuran = $_POST ['id_ukuran'];
        $nama_ukuran = $_POST['nama_ukuran'];
        $jenis_ukuran = $_POST['jenis_ukuran'];
        $harga = $_POST['harga'];
        $data_update = array(
            'nama_ukuran' => $nama_ukuran,
            'jenis_ukuran' => $jenis_ukuran,
            'harga' => $harga
        );
        $where = array('id_ukuran' => $id_ukuran);
        $res = $this->db->update('ukuran', $data_update, $where);
        if ($res >= 1) {
            //echo $res;
            $this->session->set_flashdata('pesan2', 'Data berhasil diubah!');
            redirect('admin/awal');
        } else {
            echo "gagal";
        }
    }

}
