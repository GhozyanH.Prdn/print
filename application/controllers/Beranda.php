<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('isPengguna') == TRUE) {
            $this->template->load('static','account/beranda');
        } else {
            redirect(login);
        }
	}
}
